const mix = require("laravel-mix");
const { readFileSync, writeFileSync, unlinkSync } = require("fs");
const prependFile = require("prepend-file");
const package = require("./package.json");

mix.webpackConfig(webpack => {
    return {
        externals: {
            jquery: "jQuery",
            vue: "Vue",
            dexie: "Dexie",
            undoo: "Undoo",
            store: "store"
        }
    };
})
    .js("src/index.js", "./daesirccloudsuite.user.js")
    .then(stats => {
        writeFileSync(
            "./daesirccloudsuite.meta.js",
            readFileSync("./src/meta.js", "utf8").replace(
                "VERSION_STR",
                package.version
            )
        );
        prependFile.sync(
            "./daesirccloudsuite.user.js",
            readFileSync("./src/meta.js", "utf8").replace(
                "VERSION_STR",
                package.version
            ) + "\n"
        );
        unlinkSync("./mix-manifest.json");
    });
