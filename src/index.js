import Undoo from "undoo";
import EventBus from "./eventBus";
import { settings, colorTable } from "./components";
import formatting from "./mixins/formatting";
import AliasParser from "./aliasParser";
import $chrcol from "./builtin-identifiers/chrcol";

const aliasParser = new AliasParser({
    $chrcol
});

const inputHistory = new Undoo();

var currentInput;

function createMenu() {
    return $(
        '<div id="dis-bar" class="settingsMenu__item settingsMenu__item__daesirccloudsuite"><a class="settingsMenu__link" href="/?/settings=daesirccloudsuite">Dae\'s IRCCloud Suite</a></div>'
    ).insertAfter(".settingsContainer .settingsMenu .settingsMenu__item:last");
}

function parseInput(input) {
    var inputMatches = input.val().match(/\/([a-z][a-z0-9]*)\s(.*)/i),
        matchedAlias = { command: "", text: input.val() };
    store
        .get("disAliases")
        .split("\n")
        .forEach(function(i) {
            var matches = i.match(/\/([a-z][a-z0-9]*)\s\/(say|me)\s(.*)/i);
            if (inputMatches !== null && matches !== null) {
                if (
                    inputMatches[1].toLowerCase() === matches[1].toLowerCase()
                ) {
                    matchedAlias = {
                        command: matches[2],
                        aliasText: matches[3],
                        text: inputMatches[2]
                    };
                }
            }
        });
    return matchedAlias;
}

function changeInput() {
    if (cb() == null) {
        return false;
    }

    var input = $("#bufferInputView" + cb().bid());
    currentInput = input;

    if (input.data("dis") !== 1) {
        var counter = 0;
        inputHistory.save(input.val());
        input.on("keyup", function(e) {
            if (/^.$/i.test(e.key)) {
                inputHistory.save(input.val());
            }
        });
        input.on("keydown", function(e) {
            if (e.key !== "z" && e.key !== "y" && (e.ctrlKey || e.metaKey)) {
                var handled = formatting.methods.handleCharacterInsert(
                    e.key,
                    { isVue: false, val: input.val() },
                    input[0]
                );
                if (handled) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.key === "k") {
                        var top = currentInput.offset().top,
                            left = currentInput.offset().left;
                        EventBus.$emit("numberbox-update", {
                            display: "block",
                            top:
                                top -
                                input.closest(".inputcell").outerHeight() * 2 +
                                "px",
                            left: left + "px"
                        });
                    }
                    inputHistory.save(input.val());
                }
            } else if (/^.$/i.test(e.key) && (!e.ctrlKey && !e.metaKey)) {
                counter++;
                if (counter >= 2) {
                    EventBus.$emit("numberbox-update", { display: "none" });
                    counter = 0;
                }
            } else if (
                e.key === "z" &&
                (e.ctrlKey || e.metaKey) &&
                !e.shiftKey
            ) {
                e.preventDefault();
                e.stopPropagation();
                let currentVal = input.val();
                inputHistory.undo(function(val) {
                    input.val(val);
                });
                if (/\u0003/i.test(currentVal)) {
                    EventBus.$emit("numberbox-update", { display: "none" });
                }
            } else if (
                ((e.key === "z" && e.shiftKey) || e.key === "y") &&
                (e.ctrlKey || e.metaKey)
            ) {
                e.preventDefault();
                e.stopPropagation();
                inputHistory.redo(function(val) {
                    input.val(val);
                });
            } else if (e.key === "Backspace") {
                let selectionStart = input[0].selectionStart,
                    selectionEnd = input[0].selectionEnd;
                if (selectionStart === selectionEnd) {
                    selectionEnd = selectionStart;
                    selectionStart--;
                }
                let deletedText = input
                    .val()
                    .slice(selectionStart, selectionEnd);
                if (/\u0003/i.test(deletedText)) {
                    EventBus.$emit("numberbox-update", { display: "none" });
                }
            } else if (e.key === "Enter") {
                var oldInputText = input.val(),
                    matchedAlias = parseInput(input),
                    text = matchedAlias.text,
                    parsedAlias = aliasParser.parseAlias(
                        matchedAlias,
                        oldInputText
                    ),
                    shouldPost = parsedAlias.shouldPost,
                    text = parsedAlias.text || text,
                    command = "say";
                switch (matchedAlias.command) {
                    case "say":
                        break;
                    case "me":
                        command = "me";
                        break;
                }
                if (matchedAlias.command !== "") {
                    e.preventDefault();
                    e.stopPropagation();
                    if (shouldPost) {
                        var inputHistoryLen =
                            SESSIONVIEW.mainArea.current.input.history.model
                                .inputHistory.length - 1;
                        SESSIONVIEW.mainArea.current.input[command](text);
                        SESSIONVIEW.mainArea.current.input.history.model.inputHistory.splice(
                            inputHistoryLen,
                            1
                        );
                        SESSIONVIEW.mainArea.current.input.history.model.pushInputHistory(
                            oldInputText
                        );
                    }
                }
            } else if (e.key === "Escape") {
                EventBus.$emit("numberbox-update", { display: "none" });
            }
        });
        input.data("dis", 1);
    }
}

function createSettingsPane() {
    let pane = document.createElement("div");
    pane.innerHTML = "<settings></settings>";
    $(pane).insertAfter(".settingsContentsWrapper .settingsContents:last");
    new Vue({
        el: pane,
        components: {
            settings
        }
    });
}

function init() {
    createMenu();
    changeInput();

    var hashName = "daesirccloudsuite";
    if (window.location.search === "?/settings=" + hashName) {
        SESSIONVIEW.showSettings(hashName);
    }

    createSettingsPane();
}

var disContainer = document.createElement("div");
disContainer.innerHTML = `<color-table></color-table>`;
document.querySelector("body").appendChild(disContainer);

var colorReferenceContainer = document.createElement("div");
colorReferenceContainer.className = "irccolors";
colorReferenceContainer.style.display = "none";

var logContainer = document.createElement("div");
logContainer.className = "log";

colorReferenceContainer.appendChild(logContainer);

var colorMap = {
    0: "white",
    1: "black",
    2: "navy",
    3: "green",
    4: "red",
    5: "maroon",
    6: "purple",
    7: "orange",
    8: "yellow",
    9: "lime",
    10: "teal",
    11: "cyan",
    12: "blue",
    13: "magenta",
    14: "grey",
    15: "silver"
};
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].forEach(function(color) {
    let span = document.createElement("span");
    span.className = `irccolor ${colorMap[color]}`;
    logContainer.appendChild(span);
});

document.querySelector("body").appendChild(colorReferenceContainer);

(function checkSession() {
    "use strict";

    if (window.hasOwnProperty("SESSION")) {
        window.SESSION.bind("init", function() {
            init();

            new Vue({
                el: disContainer,
                components: {
                    colorTable
                }
            });

            window.SESSION.buffers.on("doneSelected", function() {
                changeInput();
            });
        });
    } else {
        setTimeout(checkSession, 100);
    }
})();
