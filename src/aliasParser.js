export default class AliasParser {
    constructor(identifiers) {
        this.identifiers = Object.assign(
            {
                $chr(num) {
                    return String.fromCharCode(num);
                }
            },
            identifiers
        );
    }

    /**
     * Parse a function statement
     *
     * @see https://stackoverflow.com/a/28641953/785241
     * @param {string} statement
     * @returns {array}
     */
    parseStatement(statement) {
        var regex = new RegExp("(\\$[a-z]+)\\((.+)\\)", "gi");
        var results = regex.exec(statement);
        // This regex matches for the widest pattern: identifier(stuff-inside)
        // Running the previous output would result in matching groups of:
        // identifier: a
        // stuff-inside: b,c(e,f(h,i,j),g(k,l,m(o,p,q))),d(r,s,t)

        var root = [];
        // We need a way to split the stuff-inside by commas that are not enclosed in parenthesis.
        // We want to turn stuff-inside into the following array:
        // [ 'b', 'c(e,f(h,i,j),g(k,l,m(o,p,q)))', 'd(r,s,t)' ]
        // Since my regex-fu is bad, I wrote a function to do this, explained in the next step.
        if (results !== null) {
            var parameters = this.splitStatementByExternalCommas(results[2]);

            var node = {
                params: []
            };
            parameters.forEach(parameter => {
                if (parameter.indexOf("(") == -1) {
                    node.params.push(parameter.trim());
                } else {
                    // Recursion. This function produces an anonymous wrapper object around a node.
                    // I will need to unwrap my result.
                    var wrappedNode = this.parseStatement(parameter);
                    var key;
                    for (key in wrappedNode) {
                        let nodeObj = wrappedNode[key];
                        nodeObj.param = parameter;
                        node.params.push(nodeObj);
                    }
                }
            });

            // Assign node to the node's identifier
            root.push({
                param: results.input,
                call: results[0],
                function: results[1],
                params: node.params
            });
        }
        return root;
    }

    /**
     * Split a statement by commas
     *
     * @see https://stackoverflow.com/a/28641953/785241
     * @param {string} statement
     * @returns {array}
     */
    splitStatementByExternalCommas(statement) {
        statement += ","; // so I don't have to handle the "last, leftover parameter"
        var results = [];
        var chars = statement.split("");
        var level = 0; // levels of parenthesis, used to track how deep I am in ().
        var index = 0; // determines which parameter am I currently on.
        var temp = "";

        // this is somewhat like your implementation in the edits, I walk through the characters one by one, and do something extra if it's a special character.
        chars.forEach(function(char) {
            switch (char) {
                case "(":
                    temp += char;
                    level++;
                    break;
                case ")":
                    temp += char;
                    level--;
                    break;
                case ",":
                    // if the comma is between a set of parenthesis, ignore.
                    if (level !== 0) {
                        temp += char;
                    }
                    // if the comma is external, split the string.
                    else {
                        results[index] = temp;
                        temp = "";
                        index++;
                    }
                    break;
                default:
                    temp += char;
                    break;
            }
        });
        return results;
    }

    /**
     * Replace identifiers and positional args in a string
     *
     * @param {string} text
     * @param {object} textObj
     * @param {object|array} objOrArr
     * @param {string} input
     * @returns {string}
     */
    replaceArgs(text, textObj, objOrArr, input) {
        try {
            if (Array.isArray(objOrArr)) {
                objOrArr.forEach(arg => {
                    text = this.replaceArgs(text, textObj, arg, input);
                });
            } else if (typeof objOrArr === "object") {
                if (
                    typeof objOrArr.params !== "undefined" &&
                    objOrArr.params.length > 0
                ) {
                    objOrArr.params = objOrArr.params.map(param => {
                        if (typeof param === "object") {
                            let newParam = param.param.replace(
                                param.call,
                                this.identifiers[param.function](
                                    ...param.params
                                )
                            );
                            objOrArr.newCall = objOrArr.call.replace(
                                param.param,
                                newParam
                            );
                            param = newParam;
                        } else if (typeof param === "string") {
                            let positionalObj = this.parsePositionalArgs(param);
                            if (typeof positionalObj !== "undefined") {
                                param = param.replace(
                                    positionalObj.search,
                                    this.fetchArg(input, positionalObj.position)
                                );
                                textObj.shouldPost = positionalObj.required
                                    ? text.replace(/\003/i, "").trim() !== ""
                                    : true;
                            }
                        }
                        return param;
                    });
                    let newParam = objOrArr.param.replace(
                        objOrArr.call,
                        this.identifiers[objOrArr.function](...objOrArr.params)
                    );
                    objOrArr.newCall = objOrArr.call.replace(
                        objOrArr.param,
                        newParam
                    );
                    text = textObj.aliasText.replace(
                        objOrArr.call,
                        this.identifiers[objOrArr.function](...objOrArr.params)
                    );
                } else {
                    text = objOrArr.param.replace(
                        objOrArr.call,
                        this.identifiers[objOrArr.function](...objOrArr.params)
                    );
                }
            }
        } catch (e) {
            text = e.message.replace(/this.identifiers\[(.*?)\]/i, (_, $1) => {
                let objArr = $1.split("."),
                    ret = "";
                switch (objArr[0]) {
                    case "objOrArr":
                        ret = objOrArr[objArr[1]];
                        break;
                }
                return ret;
            });
            window.alert(text);
            textObj.shouldPost = false;
        }
        return text;
    }

    /**
     * Get the arg in a space-delimited string
     *
     * @param {string} str
     * @param {string|number} arg
     * @returns {string}
     */
    fetchArg(str, arg) {
        var strs = str.split(" "),
            argStart = window.parseInt((arg + "").replace(/\-\d*$/, "")),
            argEnd = window.parseInt((arg + "").replace(/\d+\-/, ""));
        if (/^0/i.test(arg)) {
            str = strs.length;
        } else if (/\-$/i.test(arg)) {
            str = strs.slice(argStart).join(" ");
        } else if (/\-\d+$/.test(arg)) {
            str = strs.slice(argStart, argEnd).join(" ");
        } else {
            str = strs[argStart];
        }
        return str;
    }

    /**
     * Parse an alias
     *
     * @param {string} aliasObj The alias object
     * @param {string} input The user input
     * @returns {string}
     */
    parseAlias(aliasObj, input) {
        let alias = {
                ...aliasObj,
                required: false
            },
            text = alias.text,
            parsedArgs = [];
        if (typeof alias.aliasText !== "undefined") {
            parsedArgs = this.parseStatement(alias.aliasText);
        }
        if (parsedArgs.length > 0) {
            text = this.replaceArgs(text, alias, parsedArgs, input);
        } else {
            let positionalArgs = this.parsePositionalArgs(alias.aliasText);
            if (typeof positionalArgs !== "undefined") {
                let fetchedArgs = this.fetchArg(input, positionalArgs.position);
                text = alias.aliasText.replace(
                    positionalArgs.search,
                    fetchedArgs
                );
                alias.shouldPost = positionalArgs.required
                    ? fetchedArgs.trim() !== ""
                    : true;
            } else {
                alias.shouldPost = true;
            }
        }
        return { text, shouldPost: alias.shouldPost };
    }

    /**
     * Get a positional arg's attributes
     *
     * @param {string} str
     * @returns {object}
     */
    parsePositionalArgs(str) {
        let positionalMatches = str.match(/(\${1,2})(\d+)(\-?)(\d*)/i);
        if (positionalMatches !== null) {
            let dollarSigns = positionalMatches[1],
                required = dollarSigns.length === 2,
                startArg = positionalMatches[2],
                willContinue = positionalMatches[3],
                endArg = positionalMatches[4];
            return {
                required,
                search: positionalMatches[0],
                position: `${startArg}${willContinue}${endArg}`
            };
        }
        return;
    }
}
