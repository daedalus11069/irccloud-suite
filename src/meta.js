// ==UserScript==
// @name         Dae's IRCCloud Suite
// @version      VERSION_STR
// @description  Provides auto quote/symbol pair coloring and aliases like mirc
// @author       Daedalus
// @include      https://www.irccloud.com/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/store.js/1.3.20/store.min.js#sha256=0jgHNEQo7sIScbcI/Pc5GYJ+VosKM1mJ+fI0iuQ1a9E=
// @require      https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js#sha256=chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=
// @require      https://unpkg.com/undoo/dist/undoo.min.js#sha384=k/+yIK/Gv7Ip999267bvW5nF+/hTv1ur/1nJRddU8P/wo0uuI5UM+51V9BTYtJJZ
// @require      https://unpkg.com/dexie@2.0.4/dist/dexie.js#sha384=W3T5JRCbdFIFlZ6ay87gCUbZ8NzkSW/qjV6aB8u1+bkD8YGWBrPtAuwPeGX+4o3l
// @grant        none
// ==/UserScript==
