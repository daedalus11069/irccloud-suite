import Vue from "vue";

export default {
    methods: {
        wrapOrInsert(textToInsert, { isVue, obj, prop, val }, ref) {
            var len = val.length,
                start = ref.selectionStart,
                end = ref.selectionEnd,
                selectedText = val.substring(start, end),
                replacement = "";
            if (start === end) {
                replacement = textToInsert;
            } else {
                replacement = textToInsert + selectedText + textToInsert;
            }
            if (isVue) {
                Vue.set(
                    obj,
                    prop,
                    val.substring(0, start) +
                        replacement +
                        val.substring(end, len)
                );
            } else {
                ref.value =
                    val.substring(0, start) +
                    replacement +
                    val.substring(end, len);
            }
            ref.setSelectionRange(start + 1, start + 1);
        },

        handleCharacterInsert(key, val, ref) {
            var handled = true;
            switch (key) {
                case "k":
                    this.wrapOrInsert("\u0003", val, ref);
                    break;
                case "b":
                    this.wrapOrInsert("\u0002", val, ref);
                    break;
                case "i":
                    this.wrapOrInsert("\u001d", val, ref);
                    break;
                case "u":
                    this.wrapOrInsert("\u001f", val, ref);
                    break;
                default:
                    handled = false;
                    break;
            }
            return handled;
        }
    }
};
