/**
 * Auto Alias Quote Colorer 1.6.0
 *
 * @param {string} alias
 * @param {string} actionColorNumber Number or \u0003
 * @param {string} speakingColorNumber Number or \u0003
 * @param {string} symbolPairs Pairs of symbols, any number possible
 * @param {string} input
 * @returns {string}
 */
export default function $chrcol() {
    var itminc = 1,
        chrchr = '"“”',
        chrali = arguments[0],
        chraccs =
            String.fromCharCode(3) +
            ((arguments[1] + "").length === 1 &&
            /^\003$/.test(arguments[1]) === false
                ? "0" + arguments[1]
                : arguments[1]),
        chrpst = arguments[arguments.length - 1],
        regEscape = /[-[\]{}()*+?.,\^$|#\s]/g,
        reg1,
        reg2,
        reg3,
        chracc,
        chrstr,
        chrspc,
        chrqm1,
        chrqm2,
        regml,
        chrchrs,
        chrchre,
        chrchrs_reg,
        chrchre_reg,
        symbolboundrys,
        symbolboundrye,
        wordchrclass,
        repreg;
    while (itminc <= arguments.length - 1) {
        if (itminc == 1) {
            chracc =
                String.fromCharCode(3) +
                ((arguments[1] + "").length === 1 &&
                /^\003$/.test(arguments[1]) === false
                    ? "0" + arguments[1]
                    : arguments[1]);
            chrspc =
                String.fromCharCode(3) +
                ((arguments[2] + "").length === 1 &&
                /^\003$/.test(arguments[1]) === false
                    ? "0" + arguments[2]
                    : arguments[2]);
            reg1 = new RegExp(
                "(\\s|^)([" + chrchr.replace(regEscape, "\\$&") + "])",
                "i"
            );
            reg2 = new RegExp(
                "([A-Za-z0-9][" +
                    chrchr.replace(regEscape, "\\$&") +
                    "]|..[" +
                    chrchr.replace(regEscape, "\\$&") +
                    "])(\\s|$)",
                "i"
            );
            if (reg1.test(chrpst)) {
                chrqm1 = chrpst.replace(
                    reg1,
                    "$1" +
                        String.fromCharCode(3) +
                        chrspc +
                        chrpst.match(reg1)[2]
                );
                if (reg2.test(chrqm1)) {
                    chrqm2 = chrqm1.replace(
                        reg2,
                        "$1" + String.fromCharCode(3) + chracc + " "
                    );
                } else {
                    chrqm2 = chrpst;
                }
            } else {
                chrqm2 = chrpst;
            }
            reg3 = new RegExp(
                "\\s([" + chrchr.replace(regEscape, "\\$&") + "])",
                "i"
            );
            while (reg3.test(chrqm2)) {
                chrqm2 = chrqm2.replace(
                    reg3,
                    " " +
                        String.fromCharCode(3) +
                        chrspc +
                        chrpst.match(reg3)[1]
                );
                while (reg2.test(chrqm2)) {
                    chrqm2 = chrqm2.replace(
                        reg2,
                        "$1" + String.fromCharCode(3) + chracc + " "
                    );
                }
            }
            chrpst = chrqm2;
            itminc += 3;
        } else {
            chrstr = arguments[itminc - 1];
            regml = chrstr.match(/^(.+?)\{text\}(.+?)$/i);
            if (regml !== null) {
                chrchrs = regml[1];
                chrchre = regml[2];
                chrchrs_reg = chrchrs
                    .replace(/([\002\035\037]|[\003][0-9,]*)/i, "")
                    .replace(regEscape, "\\$&");
                chrchre_reg = chrchre
                    .replace(/([\002\035\037]|[\003][0-9,]*)/i, "")
                    .replace(regEscape, "\\$&");
                chrspc =
                    String.fromCharCode(3) +
                    ((arguments[itminc + 2] + "").length === 1
                        ? "0" + arguments[itminc + 2]
                        : arguments[itminc + 2]);
                symbolboundrys = "(^|[\\s]|[\\002\\035\\037]|[\\003][0-9,]*)";
                symbolboundrye = "([\\s]?|[\\002\\035\\037]?|[\\003]?[0-9,]*)";
                wordchrclass =
                    "[\\s\\w\\.\\,\\'\\-\\+\\=\\:\\!\\?\\*&\\^\\%\\$\\#\\@\\002\\035\\037]";
                repreg = new RegExp(
                    symbolboundrys +
                        chrchrs_reg +
                        "(" +
                        wordchrclass +
                        "+?)" +
                        chrchre_reg +
                        symbolboundrye,
                    "gi"
                );
                chrqm2 = chrqm2.replace(
                    repreg,
                    "$1" + chrchrs + "$2" + chrchre + "$3" + chraccs
                );
            }
            itminc++;
        }
    }
    return chrali + " " + chraccs + chrqm2;
}
